const tasks = [
    {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
    {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
    {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
    {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
    {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
    {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
    {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];


const totalFrontendTasks = tasks.filter(function (task) {   //написала длинно для себя, чтобы все четко видеть
    return task.category === 'Frontend';
});
const totalFrontendTime = totalFrontendTasks.reduce((acc, item) => {
    return acc + item.timeSpent;
}, 0);

const totalBugTasks = tasks.filter(task => task.type === 'bug');
const totalBugTime = totalBugTasks.reduce((acc, item) => (acc + item.timeSpent), 0);

const UItasks = tasks.filter(task => task.title.includes('UI'));
const UItasksAmount = UItasks.length;

const frontendAndBackendTasks = tasks.reduce((acc, item) => {
    const category = item.category;

    if (category in acc) {
        acc[category]++;
    } else {
        acc[category] = 1;
    }

    return acc;
}, {});

const moreThanFourHourTasks = tasks.filter(task => task.timeSpent > 4);
const moreThanFourHourTasksList = moreThanFourHourTasks.map(task => ({title: task.title, id: task.category}));


console.log('Total frontend time is ' + totalFrontendTime + ' hours');
console.log('Total bugs time is ' + totalBugTime + ' hours');
console.log('UI tasks amount is ' + UItasksAmount);
console.log('Total frontend and backend tasks: ', frontendAndBackendTasks);
console.log('More than four hours tasks: \n', moreThanFourHourTasksList);