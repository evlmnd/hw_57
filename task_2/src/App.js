import React, {Component} from 'react';
import Form from './components/Form/Form';
import List from './components/List/List';

import './App.css';

class App extends Component {

    state = {
        items: [],
        total: 0
    };

    addItem = () => {
        const nameInput = document.getElementById('form-input-name');
        const priceInput = document.getElementById('form-input-price');
        const randomID = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

        const items = [...this.state.items];
        const item = {name: nameInput.value, price: parseInt(priceInput.value), id: randomID};

        if (isNaN(parseInt(priceInput.value))) {
            item.price = 0;
        }
        if (nameInput.value === '') {
            item.name = 'Item';
        }

        items.push(item);
        this.setState({items});

        nameInput.value = '';
        priceInput.value = '';
        this.updateTotal(items);
    };

    removeItem = (event) => {
        const items = [...this.state.items];
        const item = items.findIndex(x => ('remove' + x.id) === event.target.id);

        items.splice(item, 1);
        this.setState({items});
        this.updateTotal(items);
    };

    updateTotal = (items) => {
        const total = items.reduce((acc, item) => {
            console.log(acc);
            return acc + item.price;
        }, 0);

        this.setState({total});
    };

    render() {
        return (
            <div className="App">
                <Form click={this.addItem}/>
                <List items={this.state.items}
                    removeClick={(event) => this.removeItem(event)}  // если не передавать event, то всё ломается  :(
                    total={this.state.total}
                />

            </div>
        );
    }
}

export default App;
