import React from 'react';

import './Form.css';

const Form = props => {
    return (
        <div className="Form">
            <input type="text" id="form-input-name" placeholder="Item name"/>
            <input type="text" id="form-input-price" placeholder="Price"/>
            <button className="form-add-button" onClick={props.click}>ADD</button>
        </div>
    );
};

export default Form;