import React from 'react';

import './List.css';

const List = props => {

    const items = props.items.map((item) => (
        <li key={item.id}>
            <span className="item-name">{item.name}</span>
            <span className="item-price">{item.price} KGS</span>
            <button onClick={props.removeClick} id={'remove-' + item.id}>X</button>
        </li>
    ));
    return (
        <div className="List">
            <ul className="List-ul">
                {items}
            </ul>
            <p>Total: {props.total}</p>
        </div>
    );
};

export default List;